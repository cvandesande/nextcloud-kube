# Nextcloud Kubernetes example deployment

This is here to serve as an example to how I run Nextcloud in Kubernetes. I'm not making any guarantees that this will work for you, it's what works for me. One thing big help for me was following examples I found around the 'net. This is the culmination of my effort for my own self hosted cluster.


# Files

In the root is a single yaml final which contains all of the yaml files in manifests/. I deploy it by running `kubectl apply -f nextcloud.yaml` The files in manifest/ are just there for readability.

There is a single Dockerfile in dockerfiles, this is my take on the official Docker Nextcloud image. I prefer to run mine as a non-root user, otherwise it's pretty much identical to the Nextcloud upstream Dockerfile.

Since I run my containers as non root, and access files over NFS, I alter the userid of the php user to "82" and mount the persistent volume with the owner id of "82".

